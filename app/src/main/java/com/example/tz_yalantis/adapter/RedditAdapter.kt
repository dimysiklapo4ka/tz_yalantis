package com.example.tz_yalantis.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.tz_yalantis.R
import com.example.tz_yalantis.model.Child
import com.example.tz_yalantis.viewholder.EmptyListViewHolder
import com.example.tz_yalantis.viewholder.RedditItemViewHolder

const val EMPTY_LIST = 0
const val FULL_LIST = 1

class RedditAdapter(private var mRedditTopList: MutableList<Child>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun setDataToAdapter(mRedditTopList: MutableList<Child>?, const: Int){

        when(const){
            EMPTY_LIST -> {this.mRedditTopList = mRedditTopList}
            FULL_LIST -> {
                if (this.mRedditTopList != null) {
                    if (mRedditTopList != null) {
                        this.mRedditTopList!!.addAll(mRedditTopList)
                    }
                }else this.mRedditTopList = mRedditTopList
                }
        }
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (mRedditTopList != null){
            if ( mRedditTopList!!.size > 0) {
                FULL_LIST
            }else
                EMPTY_LIST
        }else EMPTY_LIST
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        when(i){
            EMPTY_LIST -> return EmptyListViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.view_empty_list, viewGroup, false))
            FULL_LIST -> return RedditItemViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.view_reddit_item, viewGroup, false))
        }
        return RedditItemViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.view_reddit_item, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is RedditItemViewHolder) run {
            val view: RedditItemViewHolder = viewHolder
            view.bindView(view.itemView.context, mRedditTopList!![position])
        }
    }

    override fun getItemCount(): Int {
        return mRedditTopList?.size ?: 1
    }
}
