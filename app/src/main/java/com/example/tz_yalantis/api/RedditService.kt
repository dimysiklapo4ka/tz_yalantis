package com.example.tz_yalantis.api

import com.example.tz_yalantis.model.ModelRedditTop
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RedditService {
    @GET("top.json")
    fun getTopRedits(@Query("limit") limit: Int, @Query("after") after: String) : Single<ModelRedditTop>
}