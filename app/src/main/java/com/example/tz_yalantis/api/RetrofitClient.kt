package com.example.tz_yalantis.api

import com.example.tz_yalantis.model.ModelRedditTop
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient{

    private val SERVER: String = "https://reddit.com/"

    private object Holder { val INSTANCE = RetrofitClient() }

    companion object {
        val instance: RetrofitClient by lazy { Holder.INSTANCE }
    }

    private lateinit var mRedditService: RedditService

    init {
        val retrofit = initRetrofit()
        initServices(retrofit)
    }

    private fun initRetrofit(): Retrofit {
        val gson : Gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
                .baseUrl(SERVER)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    private fun initServices(retrofit: Retrofit) {
        mRedditService = retrofit.create(RedditService::class.java)
    }

    fun getTopRedits(after: String): Single<ModelRedditTop> {
        return mRedditService.getTopRedits(10, after)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }
}