package com.example.tz_yalantis

import java.text.SimpleDateFormat
import java.util.*

fun formatDate(date: Date): String{
    val time = System.currentTimeMillis() - date.time
    val day = time / (1000 * 60 * 60 * 24)
    val hour = time / (1000 * 60 * 60)
    val minute = time / (1000 * 60) % 60
    var string = ""

    string = if (day == 0L) {

        if (hour > 0) {
            "$hour hour ago"
        } else {
            if (minute > 0) {
                "$minute minutes ago"
            } else {
                "Just now"
            }
        }
    } else {
        SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault()).format(date)
    }
    return string
}