package com.example.tz_yalantis.model

import com.google.gson.annotations.SerializedName

data class ModelRedditTop(@SerializedName("kind") val kind: String,
                          @SerializedName("data") val data: DataReddit)

data class DataReddit(@SerializedName("modhash")
                      val modhash: String,
                      @SerializedName("dist")
                      val dist: Int,
                      @SerializedName("children")
                      val children: MutableList<Child>,
                      @SerializedName("after")
                      val after: String,
                      @SerializedName("before")
                      val before: Any)

data class Child(@SerializedName("kind")
                 val kind: String,
                 @SerializedName("data")
                 val data: Data)

data class Data(@SerializedName("subreddit")
                val subreddit: String,
                @SerializedName("selftext")
                val selftext: String,
                @SerializedName("author_fullname")
                val authorFullname: String,
                @SerializedName("saved")
                val saved: Boolean,
                @SerializedName("gilded")
                val gilded: Int,
                @SerializedName("clicked")
                val clicked: Boolean,
                @SerializedName("title")
                val title: String,
                @SerializedName("subreddit_name_prefixed")
                val subredditNamePrefixed: String,
                @SerializedName("hidden")
                val hidden: Boolean,
                @SerializedName("pwls")
                val pwls: Int,
                @SerializedName("downs")
                val downs: Int,
                @SerializedName("thumbnail_height")
                val thumbnailHeight: Int,
                @SerializedName("hide_score")
                val hideScore: Boolean,
                @SerializedName("name")
                val name: String,
                @SerializedName("quarantine")
                val quarantine: Boolean,
                @SerializedName("link_flair_text_color")
                val linkFlairTextColor: String,
                @SerializedName("subreddit_type")
                val subredditType: String,
                @SerializedName("ups")
                val ups: Int,
                @SerializedName("domain")
                val domain: String,
                @SerializedName("thumbnail_width")
                val thumbnailWidth: Int,
                @SerializedName("is_original_content")
                val isOriginalContent: Boolean,
                @SerializedName("is_reddit_media_domain")
                val isRedditMediaDomain: Boolean,
                @SerializedName("is_meta")
                val isMeta: Boolean,
                @SerializedName("can_mod_post")
                val canModPost: Boolean,
                @SerializedName("score")
                val score: Int,
                @SerializedName("thumbnail")
                val thumbnail: String,
                @SerializedName("edited")
                val edited: Boolean,
                @SerializedName("post_hint")
                val postHint: String,
                @SerializedName("is_self")
                val isSelf: Boolean,
                @SerializedName("created")
                val created: Long,
                @SerializedName("link_flair_type")
                val linkFlairType: String,
                @SerializedName("wls")
                val wls: Int,
                @SerializedName("author_flair_type")
                val authorFlairType: String,
                @SerializedName("contest_mode")
                val contestMode: Boolean,
                @SerializedName("archived")
                val archived: Boolean,
                @SerializedName("no_follow")
                val noFollow: Boolean,
                @SerializedName("is_crosspostable")
                val isCrosspostable: Boolean,
                @SerializedName("pinned")
                val pinned: Boolean,
                @SerializedName("over_18")
                val over18: Boolean,
                @SerializedName("preview")
                val preview: Preview,
                @SerializedName("media_only")
                val mediaOnly: Boolean,
                @SerializedName("can_gild")
                val canGild: Boolean,
                @SerializedName("spoiler")
                val spoiler: Boolean,
                @SerializedName("locked")
                val locked: Boolean,
                @SerializedName("visited")
                val visited: Boolean,
                @SerializedName("subreddit_id")
                val subredditId: String,
                @SerializedName("link_flair_background_color")
                val linkFlairBackgroundColor: String,
                @SerializedName("id")
                val id: String,
                @SerializedName("is_robot_indexable")
                val isRobotIndexable: Boolean,
                @SerializedName("author")
                val author: String,
                @SerializedName("num_crossposts")
                val numCrossposts: Int,
                @SerializedName("num_comments")
                val numComments: Int,
                @SerializedName("send_replies")
                val sendReplies: Boolean,
                @SerializedName("whitelist_status")
                val whitelistStatus: String,
                @SerializedName("permalink")
                val permalink: String,
                @SerializedName("parent_whitelist_status")
                val parentWhitelistStatus: String,
                @SerializedName("stickied")
                val stickied: Boolean,
                @SerializedName("url")
                val url: String,
                @SerializedName("subreddit_subscribers")
                val subredditSubscribers: Int,
                @SerializedName("created_utc")
                val createdUtc: Int,
                @SerializedName("is_video")
                val isVideo: Boolean)

data class Preview(@SerializedName("images")
                   val images: MutableList<Image>,
                   @SerializedName("enabled")
                   val enabled: Boolean)

data class Image(@SerializedName("source")
                 val source: Source,
                 @SerializedName("resolutions")
                 val resolutions: MutableList<Resolution>,
                 @SerializedName("id")
                 val id: String)

data class Source(@SerializedName("url")
                  val url: String,
                  @SerializedName("width")
                  val width: Int,
                  @SerializedName("height")
                  val height: Int)

data class Resolution(@SerializedName("url")
                      val url: String,
                      @SerializedName("width")
                      val width: Int,
                      @SerializedName("height")
                      val height: Int)

