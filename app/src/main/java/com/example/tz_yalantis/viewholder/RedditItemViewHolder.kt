package com.example.tz_yalantis.viewholder

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.example.tz_yalantis.formatDate
import com.example.tz_yalantis.model.Child
import java.util.*
import kotlinx.android.synthetic.main.view_reddit_item.view.*

class RedditItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindView(context: Context, child: Child) {

        itemView.cardView.setOnClickListener(Clicked(child))

        val date = Date(child.data.createdUtc * 1000L)

        itemView.tv_time_posted.text = formatDate(date)

        Glide.with(context)
                .load(child.data.thumbnail)
                .into(itemView.iv_preview_post)

        itemView.tv_title_post.text = child.data.title

        itemView.tv_subreddit.text = "Subreddit group ${child.data.subredditNamePrefixed}"
        itemView.tv_author.text = "Posted by ${child.data.author}"

        itemView.tv_rating.text = "Rating ${child.data.score}"
        itemView.tv_comments.text = "Comments ${child.data.numComments}"
    }

    inner class Clicked(child: Child) : View.OnClickListener {
        private val url: String = child.data.url

        override fun onClick(v: View?) {
            val builder: CustomTabsIntent.Builder = CustomTabsIntent.Builder()
            builder.setToolbarColor(Color.rgb(233, 89, 37))
            val customTabsIntent: CustomTabsIntent = builder.build()
            customTabsIntent.launchUrl(itemView.context, Uri.parse(url))
        }
    }
}
