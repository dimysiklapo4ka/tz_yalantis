package com.example.tz_yalantis.activity

import com.example.tz_yalantis.base.BasePresenter
import com.example.tz_yalantis.base.BaseView
import com.example.tz_yalantis.model.Child

interface MainContract {
    interface View: BaseView{
        fun setDataToAdapter(childrens: MutableList<Child>, const: Int)
        fun enableRefresh()
    }

    interface Presenter:BasePresenter<View>{
        fun start(const: Int)
        fun isLoading():Boolean
    }
}