package com.example.tz_yalantis.activity

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ProgressBar
import com.example.tz_yalantis.R
import com.example.tz_yalantis.adapter.EMPTY_LIST
import com.example.tz_yalantis.adapter.FULL_LIST
import com.example.tz_yalantis.adapter.RedditAdapter
import com.example.tz_yalantis.base.BaseActivity
import com.example.tz_yalantis.model.Child
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainContract.View, MainContract.Presenter>(), MainContract.View {

    override var mPresenter: MainContract.Presenter = MainPresenter()
    private val mLayoutManager = LinearLayoutManager(this)
    private val mAdapter = RedditAdapter(null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.start(EMPTY_LIST)
    }

    override fun getLayoutResourse(): Int {
        return R.layout.activity_main
    }

    override fun init() {
        setColorScheme()
        mSwipeRefreshLayout.setOnRefreshListener(Refresh())
        rv_root.layoutManager = mLayoutManager
        rv_root.adapter = mAdapter
        rv_root.addOnScrollListener(Scroled())
    }

    override fun setDataToAdapter(childrens: MutableList<Child>, const: Int) {
        mAdapter.setDataToAdapter(childrens, const)
    }

    override fun enableRefresh() {
        mSwipeRefreshLayout.isRefreshing = false
    }

    override fun getProgress(): ProgressBar {
        return progressBar
    }

    inner class Refresh : SwipeRefreshLayout.OnRefreshListener {

        override fun onRefresh() {
            mPresenter.start(EMPTY_LIST)
        }

    }

    inner class Scroled : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val childCount = recyclerView.adapter!!.itemCount
            val paginationCount = childCount - 3

            if(!mPresenter.isLoading()) {
                if (childCount in 2..49) {
                    if (mLayoutManager.findLastCompletelyVisibleItemPosition() >= paginationCount) {
                        mPresenter.start(FULL_LIST)
                    }
                }
            }
        }
    }
}
