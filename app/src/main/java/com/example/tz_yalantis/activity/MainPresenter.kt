package com.example.tz_yalantis.activity

import android.util.Log
import com.example.tz_yalantis.adapter.EMPTY_LIST
import com.example.tz_yalantis.api.RetrofitClient
import com.example.tz_yalantis.base.BasePresenterImp
import com.example.tz_yalantis.model.ModelRedditTop

class MainPresenter: BasePresenterImp<MainContract.View>(),MainContract.Presenter {

    private var mAfter: String = ""
    private var isNeedRequest: Boolean = true
    private var isLoading: Boolean = false

    override fun start(const: Int) {
        apiRequest(const)
    }

    private fun setDataToAdapter(mModelRedditTop: ModelRedditTop, const: Int){
        mView!!.setDataToAdapter(mModelRedditTop.data.children, const)
        isNeedRequest(mModelRedditTop)
        mView!!.enableRefresh()
        mView!!.hideLoading()
        isLoading = false
    }

    private fun isNeedRequest(mModelRedditTop: ModelRedditTop){
        if(!mAfter.equals(mModelRedditTop.data.after)) {
            mAfter = mModelRedditTop.data.after
            isNeedRequest = true
        }else{
            isNeedRequest = false
        }
    }

    private fun errorLoging(throwable: Throwable?){
        Log.e(MainPresenter::class.java.simpleName,"ERROR ", throwable)
        mView!!.enableRefresh()
        mView!!.hideLoading()
        mView!!.showError()
    }

    private fun apiRequest(const: Int){
        when(const){
            EMPTY_LIST -> mAfter = ""
        }
        if (isNeedRequest) {
            RetrofitClient.instance
                    .getTopRedits(mAfter)
                    .doOnSubscribe { mView!!.showLoading()
                    isLoading = true}
                    .subscribe({ it -> setDataToAdapter(it, const) },
                            { t: Throwable? -> errorLoging(t) })
        }
    }

    override fun isLoading(): Boolean {
        return isLoading
    }
}