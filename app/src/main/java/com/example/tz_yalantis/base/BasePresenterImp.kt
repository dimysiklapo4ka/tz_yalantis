package com.example.tz_yalantis.base

open class BasePresenterImp<V : BaseView> : BasePresenter<V>{

    protected var mView: V? = null

    override fun attach(view: V) {
        mView = view
    }

    override fun dettach() {
        mView = null
    }

}