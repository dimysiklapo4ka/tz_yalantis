package com.example.tz_yalantis.base

interface BaseView{
    fun showLoading()
    fun hideLoading()
    fun showError()
}