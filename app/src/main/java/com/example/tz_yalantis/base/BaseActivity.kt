package com.example.tz_yalantis.base

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import com.example.tz_yalantis.R

abstract class BaseActivity<in V : BaseView, T : BasePresenter<V>>: AppCompatActivity(), BaseView {

    protected lateinit var mSwipeRefreshLayout:SwipeRefreshLayout
    abstract var mPresenter : T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(inflateWithLoadingIndicator(getLayoutResourse()))
        mPresenter.attach(this as V)
        init()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.dettach()
    }

    private fun inflateWithLoadingIndicator(resId: Int): View {
        mSwipeRefreshLayout = SwipeRefreshLayout(this)
        mSwipeRefreshLayout.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        val view = LayoutInflater.from(this).inflate(resId, null, false)
        mSwipeRefreshLayout.addView(view)
        return mSwipeRefreshLayout
    }

    protected fun setColorScheme(){
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorOrange,
                R.color.colorAccent,
                R.color.colorPrimary,
                R.color.colorPrimaryDark)
    }

    abstract fun getLayoutResourse():Int

    abstract fun init()

    abstract fun getProgress(): ProgressBar

    override fun showError() {
        Toast.makeText(this, "Server error!\nPlease try again later", Toast.LENGTH_SHORT).show()
    }

    override fun hideLoading() {
        getProgress().visibility = ViewGroup.GONE
    }

    override fun showLoading() {
        getProgress().visibility = ViewGroup.VISIBLE
    }
}