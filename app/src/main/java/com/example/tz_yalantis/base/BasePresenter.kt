package com.example.tz_yalantis.base

interface BasePresenter<in V: BaseView>{
    fun attach(view: V)
    fun dettach()
}